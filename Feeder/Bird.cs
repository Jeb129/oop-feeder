﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using WpfAnimatedGif;

namespace Feeder
{
    public class Bird
    {
        public Bird(MainWindow currentWnd, int fTime)
        {
            _CurWnd = currentWnd ?? throw new ArgumentNullException(nameof(currentWnd));
            _FeedingTime = rnd.Next(fTime - 500, fTime + 500);
            _FlightTime = rnd.Next(1500, 2500);
            (_FeedingThread ??= new(MainThread)).Start();
        }

        MainWindow _CurWnd { get; set; }
        int _FeedingTime { get; set; }
        int _FlightTime { get; set; }
        public Thread _FeedingThread { get; set; }

        static readonly Random rnd = new();

        Image _BirdGif;
        private BitmapImage GenerateImage()
        {
            BitmapImage Image = new();
            Image.BeginInit();
            Image.UriSource = new Uri(Environment.CurrentDirectory + @"\Resources\BirdFly.gif");
            Image.DecodePixelWidth = 150;
            Image.EndInit();
            return Image;
        }
        Point _StartPoint = new()
        {
            X = 780,
            Y = rnd.Next(-200, 300)
        };
        static Point _OnTreePoint = new()
        {
            X = 640,
            Y = 80
        };
        Point _OnFeederPoint = new()
        {
            X = rnd.Next(10, 130),
            Y = rnd.Next(0, 60)
        };
        static Point _EndPoint = new()
        {
            X = -200,
            Y = 150
        };

        void MainThread()
        {
            try
            {
                SpawnBird();

                _CurWnd.FeederSem?.WaitOne();

                FeedBird();
                LetBird();
            }
            catch (ThreadInterruptedException) { Thread.CurrentThread.Interrupt(); }
            catch (Exception) { }
        }

        private void LetBird()
        {
            _CurWnd.WindowPropertyEdit.WaitOne();
            _CurWnd.FeedingBirdCount--;
            _CurWnd.WindowPropertyEdit.ReleaseMutex();

            // Анимация отлёта от кормушки
            _CurWnd.Dispatcher.Invoke(() => // Запуск анимации в главном потоке
            {
                _CurWnd.FeederSem?.Release();
                MoveTo(_BirdGif, _OnFeederPoint, _EndPoint, 2000, () =>
                {
                    _BirdGif.Visibility = Visibility.Collapsed;
                });
            });
            _CurWnd.Birds.Remove(this); // Птица покормилась - себя удалила
        }

        private void FeedBird()
        {
            _CurWnd.WindowPropertyEdit.WaitOne();
            _CurWnd.QueueBirdCount--;
            _CurWnd.WindowPropertyEdit.ReleaseMutex();

            // Анимация подлёта
            _CurWnd.Dispatcher.Invoke(() => // Запуск анимации в главном потоке
            {
                _BirdGif.Visibility = Visibility.Visible;
                MoveTo(_BirdGif, _OnTreePoint, _OnFeederPoint, _FlightTime, () =>
                {
                    _CurWnd.WindowPropertyEdit.WaitOne();
                    _CurWnd.FeedingBirdCount++;
                    _CurWnd.WindowPropertyEdit.ReleaseMutex();
                });
            });

            Thread.Sleep(_FeedingTime + _FlightTime);
        }

        private void SpawnBird()
        {
            _CurWnd.Dispatcher.Invoke(() => // Запуск анимации в главном потоке
            {
                // Спавним птичку
                _BirdGif = new()
                {
                    Width = 150,
                    Height = 150,
                };
                Canvas.SetLeft(_BirdGif, _StartPoint.X);
                Canvas.SetTop(_BirdGif, _StartPoint.Y);
                Panel.SetZIndex(_BirdGif, 100);
                _CurWnd.canv.Children.Add(_BirdGif);
                ImageBehavior.SetAnimatedSource(_BirdGif, GenerateImage());

                // Прилёт на ветку
                MoveTo(_BirdGif, _StartPoint, _OnTreePoint, 2000, () =>
                {
                    _BirdGif.Visibility = Visibility.Hidden;
                    _CurWnd.WindowPropertyEdit.WaitOne();
                    _CurWnd.QueueBirdCount++;
                    _CurWnd.WindowPropertyEdit.ReleaseMutex();
                });
            });
            Thread.Sleep(2000);
        }

        public static void MoveTo(Image target, Point oldPos, Point newPos, double duration, Action onComplete)
        {
            double top = Canvas.GetTop(target);
            double left = Canvas.GetLeft(target);
            TranslateTransform trans = new TranslateTransform();
            target.RenderTransform = trans;
            DoubleAnimation anim1 = new DoubleAnimation(oldPos.Y - top, newPos.Y - top, TimeSpan.FromMilliseconds(duration));
            DoubleAnimation anim2 = new DoubleAnimation(oldPos.X - left, newPos.X - left, TimeSpan.FromMilliseconds(duration));
            int doneCount = 0;
            EventHandler com = (object? sender, EventArgs e) =>
            {
                doneCount++;
                if (doneCount == 2)
                    onComplete();
            };
            anim1.Completed += com;
            anim2.Completed += com;
            trans.BeginAnimation(TranslateTransform.YProperty, anim1);
            trans.BeginAnimation(TranslateTransform.XProperty, anim2);
        }

    }
}
