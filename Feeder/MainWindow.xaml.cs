﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace Feeder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            WindowPropertyEdit = new();
        }

        #region свойства
        int _SpawnDelay = 500;
        int _FeederCapacity = 5;
        int _FeedingTime = 2000;
        int _MaxBirdsCount = 50;
        public Semaphore? FeederSem { get; private set; }

        bool _IsWorking = false;
        public bool IsWorking
        {
            get { return _IsWorking; }
            set
            {
                if (_IsWorking != value)
                {
                    _IsWorking = value;
                    OnPropertyChanged(nameof(IsWorking));
                }
            }
        }
        public int SpawnDelay
        {
            get { return _SpawnDelay; }
            set
            {
                if (_SpawnDelay != value)
                {
                    _SpawnDelay = value <= 5000 && value >= 100 ? value : 1000;
                    OnPropertyChanged(nameof(SpawnDelay));
                }
            }
        }
        public int FeederCapacity
        {
            get { return _FeederCapacity; }
            set
            {
                if (_FeederCapacity != value)
                {
                    _FeederCapacity = value <= 100 && value >= 1 ? value : 1;
                    OnPropertyChanged(nameof(FeederCapacity));
                }
            }
        }
        public int FeedingTime
        {
            get { return _FeedingTime; }
            set
            {
                if (_FeedingTime != value)
                {
                    _FeedingTime = value <= 10000 && value >= 1000 ? value : 1000;
                    OnPropertyChanged(nameof(FeedingTime));
                }
            }
        }
        int _FeedingBirdCount = 0;
        /// <summary>
        /// Птиц в кормушке
        /// </summary>
        public int FeedingBirdCount
        {
            get { return _FeedingBirdCount; }
            set
            {
                if (_FeedingBirdCount != value)
                {
                    _FeedingBirdCount = value;
                    OnPropertyChanged(nameof(FeedingBirdCount));
                }
            }
        }
        int _QueueBirdCount = 0;
        /// <summary>
        /// Птиц на ветке
        /// </summary>
        public int QueueBirdCount
        {
            get { return _QueueBirdCount; }
            set
            {
                if (_QueueBirdCount != value)
                {
                    _QueueBirdCount = value;
                    OnPropertyChanged(nameof(QueueBirdCount));
                }
            }
        }
        public int MaxBirdsCount
        {
            get { return _MaxBirdsCount; }
            set
            {
                if (_MaxBirdsCount != value)
                {
                    _MaxBirdsCount = value;
                    OnPropertyChanged(nameof(MaxBirdsCount));
                }
            }
        }
        public Mutex WindowPropertyEdit { get; private set; }
        /// <summary>
        /// Лист птиц
        /// </summary>
        public ObservableCollection<Bird> Birds { get; private set; }
        #endregion

        public event PropertyChangedEventHandler? PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void StartStop_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            if (!IsWorking)
            {
                Birds ??= new();
                FeederSem = new(FeederCapacity, FeederCapacity);
                (new Thread(CreateBird)).Start();
            }
            else
            {
                Birds.Clear();
                OnPropertyChanged(nameof(QueueBirdCount));
            }

            IsWorking = !IsWorking;
        }
        void CreateBird()
        {
            while (IsWorking)
            {
                if (Birds.Count < MaxBirdsCount)
                {
                    Birds.Add(new(this, FeedingTime));
                    OnPropertyChanged(nameof(QueueBirdCount));
                }
                Thread.Sleep(SpawnDelay);
            }
        }
        public void StopThread(Bird bird)
        {
            bird._FeedingThread.Interrupt();
            bird._FeedingThread.Join();
        }
        private void Window_Closed(object sender, System.EventArgs e)
        {
            IsWorking = false;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            foreach (Bird bird in Birds)
            {
                StopThread(bird);
            }
        }
    }
}
